<?php

namespace Jerff\SectionProps\Model;

use Bitrix\Main\Application;
use CIBlockSection;
use CIBlockSectionPropertyLink;
use Jerff\SectionProps\Config;
use Jerff\SectionProps\Database;

class Section
{


    public static function update()
    {
        $rsParentSection = CIBlockSection::GetList([], [
            'IBLOCK_ID' => Config::CATALOG_ID,
//            'ID'        => 536,
        ]);
        while ($arParentSection = $rsParentSection->Fetch()) {
            $arFilter = [
                'IBLOCK_ID'     => $arParentSection['IBLOCK_ID'],
                '>LEFT_MARGIN'  => $arParentSection['LEFT_MARGIN'],
                '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
                '>DEPTH_LEVEL'  => $arParentSection['DEPTH_LEVEL'],
            ];
            $res = CIBlockSection::GetList(['left_margin' => 'asc'], $arFilter);
            $arSection = [$arParentSection['ID']];
            while ($arSect = $res->fetch()) {
                $arSection[] = $arSect['ID'];
            }

            $arPropLinks = CIBlockSectionPropertyLink::GetArray($arParentSection["IBLOCK_ID"], $arParentSection['ID'],
                $arParentSection['ID'] <= 0);
            $arProps = [];
            $res = Database\SectionTable::getList([
                'select' => ['PROPS_ID'],
                'filter' => [
                    'SECTION_ID' => $arParentSection['ID'],
                ],

            ]);
            while ($arItem = $res->fetch()) {
                $arProps[$arItem['PROPS_ID']] = $arItem['PROPS_ID'];
            }
            if ($arSection) {
                Database\SectionTable::startBigData();
                $res = Application::getConnection()->query('
        SELECT DISTINCT elProp.IBLOCK_PROPERTY_ID
        FROM b_iblock_section_element as secEl 
        INNER JOIN b_iblock_element_property as elProp ON (secEl.IBLOCK_ELEMENT_ID = elProp.IBLOCK_ELEMENT_ID) 
        INNER JOIN b_iblock_property as prop ON(prop.ID=elProp.IBLOCK_PROPERTY_ID) 
        WHERE prop.IBLOCK_ID=' . Config::CATALOG_ID . ' AND secEl.IBLOCK_SECTION_ID IN(' . implode(',',
                        $arSection) . ') AND prop.ACTIVE="Y"');
                while ($arItem = $res->fetch()) {
                    $arPropLink = $arPropLinks[$arItem['IBLOCK_PROPERTY_ID']];
                    if ($arPropLink['INHERITED_FROM'] != $arParentSection['ID']) {
                        $arLink = [];
                        if ($arPropLink['SMART_FILTER'] === 'Y') {
                            $arLink['SMART_FILTER'] = 'Y';
                        }
                        $arLink['DISPLAY_TYPE'] = $arPropLink['DISPLAY_TYPE'];
                        $arLink['DISPLAY_EXPANDED'] = $arPropLink['DISPLAY_EXPANDED'];
                        CIBlockSectionPropertyLink::Add($arParentSection['ID'], $arItem['IBLOCK_PROPERTY_ID'],
                            $arLink);
                    }
                    if (empty($arProps[$arItem['IBLOCK_PROPERTY_ID']])) {
                        Database\SectionTable::add([
                            'SECTION_ID' => $arParentSection['ID'],
                            'PROPS_ID'   => $arItem['IBLOCK_PROPERTY_ID'],
                        ]);
                    }
                    unset($arProps[$arItem['IBLOCK_PROPERTY_ID']]);
                }
                Database\SectionTable::compileBigData();
            }
            if ($arProps) {
                foreach ($arProps as $propsId) {
                    if ($arPropLinks[$arItem['IBLOCK_PROPERTY_ID']]['INHERITED_FROM'] == $arParentSection['ID']) {
                        CIBlockSectionPropertyLink::Delete($arParentSection['ID'], $propsId);
                    }
                }
                Database\SectionTable::queryCustom()->deleteByWhere([
                    'SECTION_ID' => $arParentSection['ID'],
                    'PROPS_ID'   => $arProps,
                ]);
            }
        }
    }


    public static function add($arSection, $propId)
    {
        preExit($arSection, $propId);
    }

    public static function delete($arSection, $propId)
    {
        preExit($arSection, $propId);
    }

}