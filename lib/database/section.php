<?php

namespace Jerff\SectionProps\Database;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;
use Jerff\SectionProps\TraitList\BigData;
use Jerff\SectionProps\TraitList\Query;

class SectionTable extends DataManager
{

    use Query;
    use BigData;
    const BIGDATA_LIMIT = 100;

    public static function getTableName()
    {
        return 'project_propsview_section';
    }

    public static function getMap()
    {
        return [
            'SECTION_ID' => new Entity\IntegerField('SECTION_ID', [
                'primary'  => true,
                'required' => true,
            ]),
            'PROPS_ID'   => new Entity\IntegerField('PROPS_ID', [
                'primary'  => true,
                'required' => true,
            ]),
        ];
    }

}